defmodule StoreHouse.Repo do
  use Ecto.Repo,
    otp_app: :store_house,
    adapter: Ecto.Adapters.Postgres
end
