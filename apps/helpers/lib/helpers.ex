defmodule Helpers do
  @moduledoc """
  Documentation for `Helpers`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Helpers.hello()
      :world

  """
  def hello do
    :world
  end
end
