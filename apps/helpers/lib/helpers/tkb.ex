defmodule Helpers.TKB do
  require Logger

  def parse_invoice(path) do
    case Helpers.Pdf2Text.run(path) do
      {:ok, out} ->
        String.split(out, "\n", trim: true)
        |> scan_invoice()
      err -> err
    end
  end

  defp scan_invoice(lines) do
    order = %{
      items: [],
      order_number: nil,
      date: nil,
      payment_method: nil,
      payment_confirmation_number: nil,
      shipping_method: nil,
      total_items: nil,
      sub_total: nil,
      shipping: nil,
      grand_total: nil
    }
    scan_header(order, lines)
  end

  defp scan_header(result, []) do
    result
  end
  defp scan_header(result, ["Order: " <> n|tl]) do
    case Integer.parse(n) do
      :error ->
        scan_header(result, tl)
      {order_num, _} ->
        scan_header(%{result | order_number: order_num}, tl)
    end
  end
  defp scan_header(result, ["Date: " <> date|tl]) do
    scan_header(%{result | date: date}, tl)
  end
  defp scan_header(result, ["Payment Method: " <> method|[conf_num|tl]]) do
    scan_header(%{result |
      payment_method: method,
      payment_confirmation_number: conf_num
      }, tl)
  end
  defp scan_header(result, ["Shipping Method: " <> method|tl]) do
    scan_header(%{result | shipping_method: method}, tl)

  end
  defp scan_header(result, ["Total Items: " <> n|tl]) do
    case Integer.parse(n) do
      :error ->
        scan_header(result, tl)
      {total_items, _} ->
        scan_header(%{result | total_items: total_items}, tl)
    end
  end
  defp scan_header(result, ["Item Price"|["Item Total"|tl]]) do
    scan_items(result, tl)
  end
  defp scan_header(result, [_hd|tl]) do
    scan_header(result, tl)
  end

  defp scan_items(result, []) do
    result
  end
  defp scan_items(result, ["Sub-total"|["$"<>n|tl]]) do
    case Float.parse(n) do
      :error ->
        scan_items(result, tl)
      {sub_total, _} ->
        scan_items(%{result | sub_total: sub_total}, tl)
    end
  end
  defp scan_items(result, ["Shipping"|["$"<>n|tl]]) do
    case Float.parse(n) do
      :error ->
        scan_items(result, tl)
      {shipping, _} ->
        scan_items(%{result | shipping: shipping}, tl)
    end
  end
  defp scan_items(result, ["Grand Total"|[_|["$"<>n|tl]]]) do
    case Float.parse(n) do
      :error ->
        scan_items(result, tl)
      {grand_total, _} ->
        scan_items(%{result | grand_total: grand_total}, tl)
    end
  end
  defp scan_items(result, ["Quantity Item"|["Item Price"|["Item Total"|tl]]]) do
    scan_items(result, tl)
  end
  defp scan_items(result, ["$"<>price|["$"<>total|[qnt|[name|[id|tl]]]]]) do
    Logger.info "#{price} #{total} #{qnt} #{name} #{id}"
    result = case create_item(qnt, name, id, price, total) do
      nil -> result
      i -> %{result | items: [i|result.items]}
    end
    scan_items(result, tl)
  end
  defp scan_items(result, [qnt|[name|[id|["$"<>price|["$"<>total|tl]]]]]) do
    Logger.info "#{qnt} #{name} #{id} #{price} #{total}"
    result = case create_item(qnt, name, id, price, total) do
      nil -> result
      i -> %{result | items: [i|result.items]}
    end
    scan_items(result, tl)
  end
  defp scan_items(result, [_hd|tl]) do
    scan_items(result, tl)
  end

  defp create_item(s_quantity, name, id, s_price, s_total) do
    with {quantity, _} <- Integer.parse(s_quantity),
      {price, _} <- Float.parse(s_price),
      {total, _} <- Float.parse(s_total) do
        %{
          name: name,
          id: id,
          quantity: quantity,
          price: price,
          total: total
        }
      else
        _ -> nil
      end
  end
end
