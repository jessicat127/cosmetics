defmodule Helpers.Pdf2Text do
  def run(path) do
    case Porcelain.exec("pdftotext", [path, "-", "-nopgbrk"]) do
      %{out: out, status: 0} -> {:ok, out}
      %{err: err, status: status} -> {:error, err, status}
    end
  end
end
